<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class UpdateOwnPostRule extends Rule
{
	public $name = 'updateOwnPostRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['post']) ? $params['post']->auther == $post : false;
		}
		return false;
	}
}