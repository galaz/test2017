<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{

	public function actionUpdateOwnPost()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\UpdateOwnPostRule;
		$auth->add($rule);
	}
	
}